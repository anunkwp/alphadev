package com.nunkung.alphadev.repository

import com.nunkung.alphadev.api.ApiService
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiService  : ApiService
) {
    suspend fun getSomeData () = apiService.getSomeData()

}