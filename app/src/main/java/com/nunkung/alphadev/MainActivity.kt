package com.nunkung.alphadev

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.nunkung.alphadev.di.SomeInterfaceImpl
import com.nunkung.alphadev.ui.AnimatedCircularProgressIndicator
import com.nunkung.alphadev.ui.ComposeCircularProgressBar
import com.nunkung.alphadev.ui.CouponShape
import com.nunkung.alphadev.ui.main.MainScreen
import com.nunkung.alphadev.ui.theme.Purple40
import com.nunkung.alphadev.ui.theme.Purple80
import com.nunkung.alphadev.ui.theme.PurpleGrey40
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@OptIn(ExperimentalMaterial3Api::class)
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @Inject
    lateinit var someString: SomeInterfaceImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("someString", someString.getSomeString())
        setContent {
            Box(
                modifier = Modifier
                    .width(300.dp)
                    .height(500.dp)
                    .background(
                        color = Color.White,
                        shape = CouponShape(50.dp, 50.dp)
                    )
                    .border(
                        width = 1.dp,
                        color = Color.Red,
                        shape = CouponShape(50.dp, 50.dp)
                    )
            )
//            MainScreen()
//            Column(modifier = Modifier.fillMaxWidth(), verticalArrangement = Arrangement.Center) {
//                AnimatedCircularProgressIndicator(
//                    currentValue = 17,
//                    maxValue = 20,
//                    progressBackgroundColor = Purple80,
//                    progressIndicatorColor = PurpleGrey40,
//                    completedColor = Purple40
//                )
//                ComposeCircularProgressBar(
//                    percentage = 0.7f,
//                    fillColor = Color(android.graphics.Color.parseColor("#4DB6AC")),
//                    backgroundColor = Color(android.graphics.Color.parseColor("#90A4AE")),
//                    strokeWidth = 10.dp
//                )
//                BottomSheetLayout()
//            }
        }
    }

//    @Composable
//    fun BottomSheetLayout() {
//        val sheetState = rememberModalBottomSheetState()
//        val scope = rememberCoroutineScope()
//        var showBottomSheet by remember { mutableStateOf(false) }
//        Scaffold(
//            floatingActionButton = {
//                ExtendedFloatingActionButton(
//                    text = { Text("Show bottom sheet") },
//                    icon = { Icon(Icons.Filled.Add, contentDescription = "") },
//                    onClick = {
//                        showBottomSheet = true
//                    }
//                )
//            }
//        ) { contentPadding ->
//            // Screen content
//            if (showBottomSheet) {
//                ModalBottomSheet(
//                    onDismissRequest = {
//                        showBottomSheet = false
//                    },
//                    sheetState = sheetState
//                ) {
//                    // Sheet content
//                    Button(onClick = {
//                        scope.launch { sheetState.hide() }.invokeOnCompletion {
//                            if (!sheetState.isVisible) {
//                                showBottomSheet = false
//                            }
//                        }
//                    }) {
//                        Text("Hide bottom sheet")
//                    }
//                }
//            }
//        }
//    }

    @Preview
    @Composable
    fun PreviewPorgressBar() {
        ComposeCircularProgressBar(
            percentage = 0.6f,
            fillColor = Color(android.graphics.Color.parseColor("#4DB6AC")),
            backgroundColor = Color(android.graphics.Color.parseColor("#90A4AE")),
            strokeWidth = 10.dp
        )
    }
}
