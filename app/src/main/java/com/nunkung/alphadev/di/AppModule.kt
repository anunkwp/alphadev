package com.nunkung.alphadev.di

import com.nunkung.alphadev.ui.navigation.AppNavigator
import com.nunkung.alphadev.ui.navigation.AppNavigatorImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun bindAppNavigator(appNavigatorImpl: AppNavigatorImpl): AppNavigator

    @Singleton
    @Binds
    abstract fun bindSomeString(someInterfaceImpl: SomeInterfaceImpl): SomeInterface
}

interface SomeInterface {
    fun getSomeString(): String
}

class SomeInterfaceImpl @Inject constructor() : SomeInterface {
    override fun getSomeString(): String {
        return "Nun Kung"
    }
}