package com.nunkung.alphadev.ui.user_detail
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun UserDetailsScreen() {
    val userDetailsViewModel = hiltViewModel<UserDetailsViewModel>()
    val viewState = userDetailsViewModel.viewState.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            IconButton(
                onClick = { userDetailsViewModel.onBackButtonClicked() }
            ) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "")
            }
        }
        Text(
            text = "UserDetailsScreen",
            style = MaterialTheme.typography.headlineMedium
        )
        Spacer(modifier = Modifier.height(32.dp))
        Text(
            text = "${viewState.firstName} ${viewState.lastName}"
        )
    }
}