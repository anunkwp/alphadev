package com.nunkung.alphadev.ui

import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.toRect
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection

class CouponShape(val cornerRadius: Dp, val holeSize: Dp) : Shape {
    override fun createOutline(size: Size, layoutDirection: LayoutDirection, density: Density): Outline {
        val rect = size.toRect()
        val cornerRadiusInPx = cornerRadius.toPx(density)
        val holeRadiusInPx = holeSize.toPx(density) / 2f
        val path = Path().apply {
            // Create path with your logic
            moveTo(cornerRadiusInPx, 0f)
            lineTo(rect.width - cornerRadiusInPx, 0f)
            arcTo(
                Rect(
                    left = rect.width - cornerRadiusInPx,
                    top = 0f,
                    right = rect.width,
                    bottom = cornerRadiusInPx,
                ),
                startAngleDegrees = 270f,
                sweepAngleDegrees = 90f,
                forceMoveTo = false
            )
            lineTo(rect.width, rect.centerRight.y - holeRadiusInPx)
            arcTo(
                Rect(
                    left = rect.centerRight.x - holeRadiusInPx,
                    top = rect.centerRight.y - holeRadiusInPx,
                    right = rect.centerRight.x + holeRadiusInPx,
                    bottom = rect.centerRight.y + holeRadiusInPx,
                ),
                startAngleDegrees = 270f,
                sweepAngleDegrees = -180f,
                forceMoveTo = false
            )
            lineTo(rect.width, rect.height - cornerRadiusInPx)
            arcTo(
                Rect(
                    left = rect.width - cornerRadiusInPx,
                    top = rect.height - cornerRadiusInPx,
                    right = rect.width,
                    bottom = rect.height,
                ),
                startAngleDegrees = 0f,
                sweepAngleDegrees = 90f,
                forceMoveTo = false
            )
            lineTo(cornerRadiusInPx, rect.height)
            arcTo(
                Rect(
                    left = 0f,
                    top = rect.height - cornerRadiusInPx,
                    right = cornerRadiusInPx,
                    bottom = rect.height,
                ),
                startAngleDegrees = 90f,
                sweepAngleDegrees = 90f,
                forceMoveTo = false
            )
            lineTo(0f, rect.centerLeft.y + holeRadiusInPx)
            arcTo(
                Rect(
                    left = -holeRadiusInPx,
                    top = rect.centerLeft.y - holeRadiusInPx,
                    right = holeRadiusInPx,
                    bottom = rect.centerLeft.y + holeRadiusInPx,
                ),
                startAngleDegrees = 90f,
                sweepAngleDegrees = -180f,
                forceMoveTo = false
            )
            lineTo(0f, cornerRadiusInPx)
            arcTo(
                Rect(
                    left = 0f,
                    top = 0f,
                    right = cornerRadiusInPx,
                    bottom = cornerRadiusInPx,
                ),
                startAngleDegrees = 180f,
                sweepAngleDegrees = 90f,
                forceMoveTo = false
            )
            close()
        }
        return Outline.Generic(path)
    }

    private fun Dp.toPx(density: Density) = with(density) { this@toPx.toPx() }
}