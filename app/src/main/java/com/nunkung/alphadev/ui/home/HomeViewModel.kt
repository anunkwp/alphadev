package com.nunkung.alphadev.ui.home

import androidx.lifecycle.ViewModel
import com.nunkung.alphadev.ui.navigation.AppNavigator
import com.nunkung.alphadev.ui.navigation.Destination
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val appNavigator: AppNavigator
) : ViewModel() {

    fun onNavigateToUsersButtonClicked() {
        appNavigator.tryNavigateTo(Destination.UsersScreen())
    }

    fun onNavigateToMessagesButtonClicked() {
        appNavigator.tryNavigateTo(Destination.MessagesScreen())
    }

    fun onNavigateToDetailsButtonClicked() {
        appNavigator.tryNavigateTo(Destination.DetailsScreen())
    }
}