package com.nunkung.alphadev.ui.main

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.nunkung.alphadev.ui.home.HomeScreen
import com.nunkung.alphadev.ui.messages.MessagesScreen
import com.nunkung.alphadev.ui.navigation.Destination
import com.nunkung.alphadev.ui.navigation.NavigationIntent
import com.nunkung.alphadev.ui.theme.AlphaDevTheme
import com.nunkung.alphadev.ui.user_detail.UserDetailsScreen
import com.nunkung.alphadev.ui.users.UsersScreen
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import com.nunkung.alphadev.ui.navigation.NavHost
import com.nunkung.alphadev.ui.navigation.composable
import androidx.hilt.navigation.compose.hiltViewModel
import com.nunkung.alphadev.ui.CouponShape

@Composable
fun MainScreen() {
    val navController = rememberNavController()
    val mainViewModel = hiltViewModel<MainViewModel>()
    NavigationEffects(
        navigationChannel = mainViewModel.navigationChannel,
        navHostController = navController
    )
    AlphaDevTheme {

        Column(
            modifier = Modifier.fillMaxWidth().padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                NavHost(
                    navController = navController,
                    startDestination = Destination.HomeScreen
                ) {
                    composable(destination = Destination.HomeScreen) {
                        HomeScreen()
                    }
                    composable(destination = Destination.UsersScreen) {
                        UsersScreen()
                    }
                    composable(destination = Destination.UserDetailsScreen) {
                        UserDetailsScreen()
                    }
                    composable(destination = Destination.MessagesScreen) {
                        MessagesScreen()
                    }
                    composable(destination = Destination.DetailsScreen) {
                        Destination.DetailsScreen()
                    }
                }
            }

        }
    }
}

@Composable
fun NavigationEffects(
    navigationChannel: Channel<NavigationIntent>,
    navHostController: NavHostController
) {
    val activity = (LocalContext.current as? Activity)
    LaunchedEffect(activity, navHostController, navigationChannel) {
        navigationChannel.receiveAsFlow().collect { intent ->
            if (activity?.isFinishing == true) {
                return@collect
            }
            when (intent) {
                is NavigationIntent.NavigateBack -> {
                    if (intent.route != null) {
                        navHostController.popBackStack(intent.route, intent.inclusive)
                    } else {
                        navHostController.popBackStack()
                    }
                }

                is NavigationIntent.NavigateTo -> {
                    navHostController.navigate(intent.route) {
                        launchSingleTop = intent.isSingleTop
                        intent.popUpToRoute?.let { popUpToRoute ->
                            popUpTo(popUpToRoute) { inclusive = intent.inclusive }
                        }
                    }
                }
            }
        }
    }
}