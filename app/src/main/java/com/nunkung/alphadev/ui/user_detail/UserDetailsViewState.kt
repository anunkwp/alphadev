package com.nunkung.alphadev.ui.user_detail


data class UserDetailsViewState(
    val firstName: String = "",
    val lastName: String = ""
)