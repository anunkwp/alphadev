package com.nunkung.alphadev.ui
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.nunkung.alphadev.ui.theme.AlphaDevTheme
import kotlinx.coroutines.delay

@Composable
fun CircularProgressBar(
    modifier: Modifier = Modifier,
    progress: Float = 0.5f,
    progressBarColor: Color = Color.Green,
    backgroundColor: Color = Color.Gray,
    strokeWidth: Dp = 8.dp
) {
    val stroke = with(LocalDensity.current) { strokeWidth.toPx() }
    val padding = stroke / 2

    Box(
        modifier = modifier
            .background(backgroundColor)
            .padding(padding.dp)
    ) {
        Canvas(
            modifier = Modifier
                .fillMaxSize()
        ) {
            val radius = size.minDimension / 2
            val startAngle = -90f
            val sweepAngle = progress * 360

            // Draw background circle

            drawArc(
                color = progressBarColor.copy(alpha = 0.3f),
                startAngle = startAngle,
                sweepAngle = 360f,
                useCenter = false,
            )

            // Draw progress circle
            drawArc(
                color = progressBarColor,
                startAngle = startAngle,
                sweepAngle = sweepAngle,
                useCenter = false,
            )
        }
    }
}

@Composable
fun CircularProgressBarWithState() {
    var progress by remember { mutableStateOf(0.5f) }

    CircularProgressBar(
        progress = progress,
        progressBarColor = Color.Green,
        backgroundColor = Color.Gray,
        strokeWidth = 12.dp,
        modifier = Modifier
            .size(200.dp)
            .padding(16.dp)
    )

    // Simulate progress change over time
    LaunchedEffect(key1 = progress) {
        for (i in 0..100) {
            delay(20)
            progress = i / 100f
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CircularProgressBarPreview() {
    AlphaDevTheme {
        CircularProgressBarWithState()
    }
}