package com.nunkung.alphadev.ui.users

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun UsersScreen(

) {
    val usersViewModel = hiltViewModel<UsersViewModel>()
    val viewState = usersViewModel.viewState.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            IconButton(
                onClick = { usersViewModel.onBackButtonClicked() }
            ) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "")
            }
        }
        Text(
            text = "UsersScreen",
            style = MaterialTheme.typography.headlineMedium
        )
        Spacer(modifier = Modifier.height(32.dp))
        LazyColumn(
            modifier = Modifier.weight(1f)
        ) {
            items(viewState.users) {
                UserRow(user = it) { user ->
                    usersViewModel.onUserRowClicked(user)
                }
            }
        }
    }
}

@Composable
private fun UserRow(
    user: User,
    onUserClick: (User) -> Unit
) {
    Spacer(modifier = Modifier.height(16.dp))
    Card(
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable { onUserClick(user) }
                .padding(16.dp)
        ) {
            Text(text = "${user.firstName} ${user.lastName}")
        }
    }
}