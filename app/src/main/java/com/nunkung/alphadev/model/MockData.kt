package com.nunkung.alphadev.model

data class MockData(
    val name : String = "",
    val title : String = ""
)
