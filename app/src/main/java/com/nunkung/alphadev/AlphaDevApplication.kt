package com.nunkung.alphadev

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AlphaDevApplication : Application() {
}